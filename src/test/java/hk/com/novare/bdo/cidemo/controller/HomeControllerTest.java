package hk.com.novare.bdo.cidemo.controller;

import hk.com.novare.bdo.cidemo.service.impl.HomeServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest()
public class HomeControllerTest {

    @InjectMocks
    private HomeController homeController;

    @MockBean
    private HomeServiceImpl homeService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void home() throws Exception {
        // given
        when(homeService.getHomeString()).thenReturn("HOME!");

        // when
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // then
        String expected = "HOME!";
        Assert.assertEquals(expected, result.getResponse().getContentAsString());
        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    public void getNewString() throws Exception {
        // given
        when(homeService.getNewString()).thenReturn("NEW");

        // when
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/new")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // then
        String expected = "NEW";
        Assert.assertEquals(expected, result.getResponse().getContentAsString());
        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }
}