package hk.com.novare.bdo.cidemo.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class HomeServiceImplTest {

    @InjectMocks
    private HomeServiceImpl homeService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getHomeString() {
        // given
        String expectedString = "Sample String";
        homeService.homeString = expectedString;

        // when
        String actualString = homeService.getHomeString();

        // then
        assertEquals(expectedString, actualString);
    }

    @Test
    public void getNewString() {
        // given
        String expectedString = "newString";

        // when
        String actualString = homeService.getNewString();

        // then
        assertEquals(expectedString, actualString);

    }
}