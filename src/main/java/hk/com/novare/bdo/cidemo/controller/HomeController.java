package hk.com.novare.bdo.cidemo.controller;

import hk.com.novare.bdo.cidemo.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class HomeController {

    private final HomeService homeService;

    @Autowired
    public HomeController(HomeService homeService) {
        this.homeService = homeService;
    }

    @GetMapping
    public String home() {
        return homeService.getHomeString();
    }

    @GetMapping("/new")
    public String getNewString() {
        return homeService.getNewString();
    }


}
