package hk.com.novare.bdo.cidemo.service.impl;

import hk.com.novare.bdo.cidemo.service.HomeService;
import org.springframework.stereotype.Service;

@Service
public class HomeServiceImpl implements HomeService {

    public String homeString = "I'm Home";

    @Override
    public String getHomeString() {
        return homeString;
    }

    @Override
    public String getNewString() {
        return "newString";
    }
}
