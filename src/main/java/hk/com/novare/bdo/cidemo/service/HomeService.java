package hk.com.novare.bdo.cidemo.service;

public interface HomeService {

    String getHomeString();

    String getNewString();
}
