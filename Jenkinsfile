pipeline {
    agent any
    tools {
        jdk 'Java 8u101'
        maven 'maven-3.2.2'
    }
    options {
      gitLabConnection('Gitlab')
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }
    stages {
      stage('Initialization') {
          steps {
            updateGitlabCommitStatus name: 'Initialization', state: 'pending'
          }
          post {
            always {
              updateGitlabCommitStatus name: 'Initialization', state: 'success'
            }
          }
      }
      stage("Build") {
        steps {
          updateGitlabCommitStatus name: 'Build', state: 'pending'
          sh 'mvn -B -DskipTests clean install package'
        }
        post {
            failure {
                updateGitlabCommitStatus name: 'Build', state: 'failed'
            }
            success {
                updateGitlabCommitStatus name: 'Build', state: 'success'
            }

        }
      }
      stage('Test') {
        steps {
            updateGitlabCommitStatus name: 'Test', state: 'pending'
            withSonarQubeEnv('SonarQube') {
                sh "mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent test package org.sonarsource.scanner.maven:sonar-maven-plugin:3.5.0.1254:sonar"
            }
        }
        post {
            always {
                junit 'target/surefire-reports/*.xml'
            }
            failure {
                updateGitlabCommitStatus name: 'Test', state: 'failed'
            }
            success {
                updateGitlabCommitStatus name: 'Test', state: 'success'
            }
        }
      }
    }
}